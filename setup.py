from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='plantuml-parser',
    version='0.0.1',
    description='Parser for a PlantUML like language',
    long_description=readme,
    author='Michael Sieber',
    url='https://gitlab.com/low_code/plantuml-parser',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)