# PlantUML Parser

The PlantUML parser reads PlantUML files and creates an object tree wich can be used for further processing.
This parser only supports class diagrams.

## Developer Setup

For development the following setup is recommended:

* Visual Studio Code with the following plugins:
  * ms-python.python
  * jebbs.plantuml
* Python >= 3.8

Start developing:
* pip3 install -r requirements.txt

Run unit tests:
* python -m unittest discover tests/

Run a single test:
* python -m unittest tests.test_parser.Test_Parser.test_empty_class

## Syntax

PlantUML offers quite plenty of variations how to write UML class diagrams. For easier parsing and handling, a specific syntax of the PlantUML language is necessary.
Therefor a language description with the allowed syntax is provided here.

## General

Syntax:

```
@startuml
...
@enduml
```

## Classes

Syntax:

```class <class_name> {}```

Example:

```
@startuml
class MyClass {}
@enduml
```

```plantuml
@startuml
class MyClass {}
@enduml
```

### Visibility

When you define methods or fields, you can use characters to define the visibility of the corresponding item:

|Character|Visibility|
|---|---|
|-|private|
|#|protected|
|+|public|

### Fields

Syntax: 

```<visibility> <field_name> : <datatype>```

Example:

```
@startuml
class MyClass {
  - field1 : String
  # field2 : Integer
  + field3 : Date
}
@enduml
```

```plantuml
@startuml
class MyClass {
  - field1 : String
  # field2 : Integer
  + field3 : Date
}
@enduml
```

### Methods

Syntax: 

```<visibility> <return_type> <method_name>()```

```<visibility> <return_type> <method_name>(<param_name> : <datatype>, ...)```

Example:

```
@startuml
class MyClass {
  - void method1()
  # String method2(param1 : Integer)
  + Integer method3(param1 : Integer, param2 : String)
}
@enduml
```

```plantuml
@startuml
class MyClass {
  - void method1()
  # String method2(param1 : Integer)
  + Integer method3(param1 : Integer, param2 : String)
}
@enduml
```

### Relationships

#### Cardinality

These symbols indicate the number of instances of one class linked to one instance of the other class. Possible cardinalities 

|Cardinality|Description|
|---|---|
|*|Zero or more|
|n|Only n (where n > 0)|

#### Association

Syntax: 

```<class_name_1> "<cardinality>" --> "<cardinality>" <class_name_2>```

Example:

```
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 "*" --> "1" MyClass2
@enduml
```

```plantuml
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 "*" --> "1" MyClass2
@enduml
```

#### Extension

Syntax: 

```<class_name_1> --|> <class_name_2>```

Example:

```
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 --|> MyClass2
@enduml
```

```plantuml
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 --|> MyClass2
@enduml
```

#### Composition

Syntax: 

```<class_name_1> *-- <class_name_2>```

Example:

```
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 *-- MyClass2
@enduml
```

```plantuml
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 *-- MyClass2
@enduml
```

#### Aggregation

Syntax: 

```<class_name_1> o-- <class_name_2>```

Example:

```
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 o-- MyClass2
@enduml
```

```plantuml
@startuml
class MyClass1 {}
class MyClass2 {}

MyClass1 o-- MyClass2
@enduml
```
