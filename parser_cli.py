#!/usr/bin/env python
import argparse
from plantuml import parser

__version__ = '0.0.1'
__author__ = u'Michael Sieber'

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('PlantUML Parser')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--file', '-f', type=argparse.FileType('r', encoding='UTF-8'),
                        required=True,
                        help='Specifies the PlantUML file which should get parsed')
    return parser

def main(args=None):
    """
    Main entry point of the PlantUML Parser.
    """

    argparser = get_parser()
    args = argparser.parse_args(args)

    classDiagramParser = parser.ClassDiagramParser()
    classDiagramParser.parse(args.file.read())


if __name__ == '__main__':
    main()
