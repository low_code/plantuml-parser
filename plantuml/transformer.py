from lark import Transformer
from plantuml import models

class ClassDiagramTransformer(Transformer):
    """Parse class diagrams
    
    Transform the AST from the lark parser to objects.
    """
    
    def clazz(self, args):
        """Transfrom the 'class' rule to a Class object

        Args:
            args: The values of the rule
        """
        clazz = models.Class()
        clazz.name = args[0].value

        for item in args[1].children:
            if isinstance(item, models.Field):
                clazz.fields.append(item)
            if isinstance(item, models.Method):
                clazz.methods.append(item)

        return clazz

    def field(self, args):
        """Transfrom the 'field' rule to a Field object

        Args:
            args: The values of the rule
        """
        field = models.Field()
        field.visibility = args[0].value
        field.name = args[1].value
        field.datatype = args[2].value
        return field

    def method(self, args):
        """Transfrom the 'method' rule to a Method object

        Args:
            args: The values of the rule
        """
        method = models.Method()
        method.visibility = args[0].value
        method.returntype = args[1].value
        method.name = args[2].value

        for param in args:
            if isinstance(param, models.Parameter):
                method.params.append(param)

        return method

    def parameter(self, args):
        """Transfrom the 'parameter' rule to a Parameter object

        Args:
            args: The values of the rule
        """
        param = models.Parameter()
        param.name = args[0].value
        param.datatype = args[1].value
        return param

    def relationship(self, args):
        """Transfrom the 'relationship' rule to a Relation object

        Args:
            args: The values of the rule
        """
        relation = models.Relation()
        relation.source = args[0].value
        relation.source_cardinality = args[1].value[1]
        relation.type = args[2].value
        relation.target_cardinality = args[3].value[1]
        relation.target = args[4].value
        return relation