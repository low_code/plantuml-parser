from lark import Lark
from plantuml import transformer
from plantuml import models

class ClassDiagramParser:
    """PlantUML class diagram parser.

    This class parses a subset stricter version of the PlantUML class diagram language
    and converts it to a list of Class objects for further processing.

    Attributes:
        grammar (string): The grammar for parsing class diagrams written in lark
        parser (Lark): The lark parser for the grammar

    """

    __grammar = r"""
        start: "@startuml" (clazz | relationship)+ "@enduml"

        clazz: "class" IDENTIFIER "{" class_body "}"

        class_body: (field | method)*

        field: visibility IDENTIFIER ":" datatype

        method: visibility returntype IDENTIFIER "(" parameter? ("," parameter)* ")"

        parameter: IDENTIFIER ":" datatype

        relationship: IDENTIFIER CARDINALITY? rtype CARDINALITY? IDENTIFIER

        ?rtype: ASSOCIATION | EXTENSION | COMPOSITION | AGGREGATION

        ?visibility: PRIVATE | PROTECTED | PUBLIC

        ?datatype: STRING | INTEGER | DATE | TIME | DATETIME | BOOLEAN | CHAR | DECIMAL

        ?returntype: VOID | datatype

        NUMBER_WITHOUT_ZERO: "1".."9"
        CARDINALITY: /"([0-9]+|\*)"/
        PRIVATE: "-"
        PROTECTED: "#"
        PUBLIC: "+"
        ASSOCIATION: "-->"
        EXTENSION: "--|>"
        COMPOSITION: "*--"
        AGGREGATION: "o--"
        STRING: "String"
        INTEGER: "Integer"
        DATE: "Date"
        TIME: "Time"
        DATETIME: "DateTime"
        BOOLEAN: "Boolean"
        CHAR: "Char"
        DECIMAL: "Decimal"
        VOID: "void"

        %import common.LETTER
        %import common.CNAME -> IDENTIFIER
        %import common.INT -> NUMBER
        %import common.WS

        %ignore WS
    """

    __parser = None

    def __init__(self):
        """Initialize the parser by instantiating the lark parser
        with the grammar.
        """
        self.__parser = Lark(self.__grammar)

    def __process(self, tree):
        """Traverses and processes the given parsing tree.

        Args:
            tree (Tree): The parsed tree which should get traversed

        Returns:
            Class[]: A list of classes which got read out of the parsing tree

        """
        if not tree:
            return []

        transformed = transformer.ClassDiagramTransformer().transform(tree)
        classes = self.__resolve_relations(transformed.children)
        return classes

    def __resolve_relations(self, tree):
        """Add the relations to the correct class.

        Checks all relations and adds them to the correct class or creates an
        empty class if the source or target class of the relation does not exist.

        Args:
            tree: The object tree with the classes and relationships to resolve

        Returns:
            Class[]: A list of classes with the correct relationships assigned
        
        """
        classes = { clazz.name : clazz for clazz in tree if isinstance(clazz, models.Class)}
        relations = [relation for relation in tree if isinstance(relation, models.Relation)]

        for relation in relations:
            if relation.source not in classes:
                c = models.Class()
                c.name = relation.source
                classes[c.name] = c

            if relation.target not in classes:
                c = models.Class()
                c.name = relation.target
                classes[c.name] = c

            classes[relation.source].relations.append(relation)

        return list(classes.values())

    def parse_file(self, file):
        """Parse the given file.

        Args:
            file (string): Path to the file which should get parsed
        
        Returns:
            Class: The Class object from the parsed file or an emtpy list if the file is empty
        """

        with open(file, 'r') as f:
            diagram = f.read()
            return self.parse(diagram)
        
        return []

    def parse(self, diagram):
        """Parse the given diagram string.

        Args:
            diagram (string): The string containing the class diagram to parse

        Returns:
            Class: The Class object from the parsed string or an emtpy list if the file is empty
        """

        if not diagram:
            return []

        tree = self.__parser.parse(diagram)
        return self.__process(tree)